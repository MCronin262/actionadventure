﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEngine.UI;

public class FileSelectController : MonoBehaviour {

    public Text nameText;
    public Text statsText;
    public GameObject deleteButton;

    private void OnEnable()
    {
        if (SaveFileExists())
        {
            SaveManager.SaveFile saveFile = SaveManager.Read(transform.GetSiblingIndex());
            nameText.text = saveFile.name;
            statsText.text = string.Format("HP: {0}/{1} \n LVL: {2}", saveFile.health,saveFile.maxHealth,saveFile.scene);
            deleteButton.SetActive(true);
        }
        else
        {
            nameText.text = "Start New Game";
            statsText.text = "";
            deleteButton.SetActive(false);
        }
    }

    bool SaveFileExists()
    {
        int fileID = transform.GetSiblingIndex();
        return (SaveManager.Read(fileID)!=null);
    }

    public void ButtonClicked()
    {
        Debug.Log("SAVE? " + SaveFileExists());
        if (SaveFileExists())
        {
            SaveManager.Load(transform.GetSiblingIndex());
        }
        else
        {
            MainMenuManager.instance.ShowNewGameScreen();
            NameEntryController.instance.fileID = transform.GetSiblingIndex();
        }
    }
    
    
    public void DeleteSaveFile()
    {
        MainMenuManager.instance.PromptDelete(this);
    }

    public void ConfirmDelete()
    {
        SaveManager.Delete(transform.GetSiblingIndex());
        nameText.text = "New Game";
        statsText.text = "";
        deleteButton.SetActive(false);
       // PlayerPrefs.Save();
    }
}
