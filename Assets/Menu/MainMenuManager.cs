﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour {
    public static MainMenuManager instance;

    public RectTransform startScreen;
    public RectTransform fileSelectScreen;
    public RectTransform newGameScreen;

    public Selectable startScreenSelectable;
    public Selectable fileSelectScreenSelectable;
    public Selectable newGameScreenSelectable;

    public enum Screen
    {
        Start,
        FileSelect,
        NewGame
    }

    public Screen currentScreen=Screen.Start;

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (currentScreen == Screen.Start)
            {
                Application.Quit();
            }
            else
            {
                switch (currentScreen)
                {
                    case Screen.FileSelect:
                        ShowStartScreen();
                        break;
                    case Screen.NewGame:
                        ShowFileSelectScreen();
                        break;
                }
            }
        }
    }

    private void Awake()
    {
        if (instance == null) { instance = this; }
        ShowStartScreen();
        deleteFilePrompt.SetActive(false);
    }

    public void ShowStartScreen()
    {
        EventSystem.current.SetSelectedGameObject(null);
        startScreenSelectable.Select();
        currentScreen = Screen.Start;
        
        startScreen.gameObject.SetActive(true);
        fileSelectScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(false);
        
    }

    public void ShowFileSelectScreen()
    {
        EventSystem.current.SetSelectedGameObject(null);
        fileSelectScreenSelectable.Select();
        currentScreen = Screen.FileSelect;
        startScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(true);
        newGameScreen.gameObject.SetActive(false);
        
    }

    public void ShowNewGameScreen()
    {
        EventSystem.current.SetSelectedGameObject(null);
        currentScreen = Screen.NewGame;
        startScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(true);
        
    }

    private FileSelectController fileSelect;
    public GameObject deleteFilePrompt;
    public void PromptDelete(FileSelectController fileSelect)
    {
        this.fileSelect = fileSelect;
        deleteFilePrompt.SetActive(true);
    }
    

    public void ConfirmDelete(bool confirm)
    {
        deleteFilePrompt.SetActive(true);
        if (confirm)
        {
            fileSelect.ConfirmDelete();
            deleteFilePrompt.SetActive(false);
        }
        else
        {
            deleteFilePrompt.SetActive(false);
        }
    }
}
