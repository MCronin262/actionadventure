﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WizardController : MonoBehaviour
{

    Rigidbody rb;

    Transform player;
    Animator anim;

    public GameObject attackBox;
    
    public float chaseDist = 5;
    public float attackDist = 1;
    public float speed = 2;
    public float attackStart;
    public float turnSpeed = 2;
    public HealthController health;

    public bool turnsRed = false;
    public GameObject smoke;

    int phase = 1;
    bool resting=false;
    public Transform origin;
    int fired = 0;
    public bool yowch=false;

    IEnumerator b ;


    private float timer;
    private float timeActual=15;

    //Color blue = Color.blue;
    Color blue = new Color32(0x00, 0x11, 0x85, 0xFF);
    Color red = new Color32(0x00, 0xFF, 0xF4, 0xFF);

    Color white = Color.white;

    Vector3 knockback;

    public enum State
    {
        Idle,
        Chase,
        Attack,
        Recovering,
        Dead,
            Recharge
    }
    public State state = State.Idle;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = PlayerController.instance.transform;
        anim = GetComponent<Animator>();
        health = GetComponent<HealthController>();
        smoke.SetActive(false);
       //  b = (Recharged());
        timer = timeActual;
    }

    void OnEnable()
    {
        if (health == null) Debug.Log("Health is null");
        health.onHealthChanged += HealthChanged;
    }

    void OnDisable()
    {
        health.onHealthChanged -= HealthChanged;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Chase:
                ChaseUpdate();
                break;
            case State.Attack:
            //      AttackUpdate();
           
                break;
            case State.Recovering:
                RecoveringUpdate();
                break;
            case State.Dead:
                DeadUpdate();
                break;
            case State.Recharge:
                RechargeUpdate();
                break;
            default:
                Debug.LogWarning("Undefined enemy state");
                break;

        }

        if (!PlayerController.instance.weakspot) { gameObject.layer = 13; }
        else { gameObject.layer = 8; }

        if (resting == true)
        {
            timer -= Time.deltaTime;



            if (timer < 0)
            {

                anim.SetTrigger("ready");
                state = State.Idle;
                resting = false;
                fired = 0;
                Debug.Log("Heyyo");

                timer = timeActual;
            }
        }
    }

    void IdleUpdate()
    {
        anim.ResetTrigger("attack");
        rb.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.position);
        Vector3 dir = (player.position - transform.position).normalized;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, dir, speed, 0.0f);
        transform.localRotation = Quaternion.LookRotation(newDir);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
        // transform.LookAt(player);
        if (dist < chaseDist)
        {
           // anim.SetTrigger("ready");
            state = State.Chase;
        }
    }
    void ChaseUpdate()
    {

        float dist = Vector3.Distance(transform.position, player.position);

        Vector3 dir = (player.position - transform.position).normalized;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, dir, speed, 0.0f);
        transform.localRotation = Quaternion.LookRotation(newDir);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);

        //rb.velocity = -transform.forward * speed*100;

         rb.AddForce(-transform.forward * 200);

        Vector3 cross = Vector3.Cross(-transform.forward, dir);
        transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);
        rb.velocity = -dir * speed*10000;
       // transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);


        if (dist > chaseDist)
        {
            state = State.Idle;
           // anim.SetTrigger("unReady");
        }
        else if (dist < attackDist)
        {
            if (phase == 1)
            {
                if (fired == 3) {
                   
                    state = State.Recharge;
                }
                else {anim.SetTrigger("attack"); }
                
            }

           else if (phase==2)
            {
                if (fired == 6)
                {

                    state = State.Recharge;
                }
                else { anim.SetTrigger("attack"); }
            }

            else
            {
                if (fired == 8)
                {

                    state = State.Recharge;
                }
                else
                {
                    anim.SetTrigger("redAttack");
                }
                
            }

           // rb.velocity = Vector3.zero;
           // state = State.Attack;
            //attackStart = Time.time;
            
        }
        else
        {

          //  rb.velocity = dir * speed;
        }
    }
   

    void RechargeUpdate()
    {        
        if (!resting)
        {
            resting = true;
            anim.SetTrigger("unReady");
           // StartCoroutine(b);
        }

      //  if (resting == true)
      //  {
      //      timer -= Time.deltaTime;
      //
      //      
      //
      //      if (timer < 0)
      //      {
      //                          
      //          anim.SetTrigger("ready");
      //          state = State.Idle;
      //          resting = false;                
      //          fired = 0;
      //          Debug.Log("Heyyo");   
      //         
      //          timer = timeActual;
      //      }
      //  }
    }


    void RecoveringUpdate()
    {

        if (health.health == 6)
        {
            phase = 2;
            
            smoke.SetActive(true);
        }
        if (health.health == 3)
        {
            phase = 3;
            turnsRed = true;
            StartCoroutine(RedLightChange());
            
        }
    }


    void DeadUpdate()
    {
        chaseDist = 0;
        attackDist = 0;
        gameObject.layer = 12;
    }

    public void AttackFinished()
    {
        state = State.Idle;   

    }

    public void ShootMissile()
    {
        GameObject missile = Spawner.instance.Spawn("missile");
        missile.transform.position = origin.transform.position;
        AudioManager.instance.PlaySFX("wizFlame");
        fired++;
        missile.GetComponent<MissileController>().Sombrero(origin.forward);
    }


    public void RedAttack()
    {
        AudioManager.instance.PlaySFX("redAttack");
        fired++;
    }


        public void Yowch()
    {
       // state = State.Idle;
        attackBox.SetActive(false);
    }

    void HealthChanged(float previousHealth, float health)
    {
        if (previousHealth > 0 && health == 0)
        {
            anim.SetTrigger("death");
            state = State.Dead;
            AudioManager.instance.PlaySFX("Spook");
            StartCoroutine(LightChange());
            knockback = rb.transform.position - PlayerController.instance.rb.transform.position;
            PlayerController.instance.rb.AddForce(-knockback * 250, ForceMode.Force);
            smoke.SetActive(false);
            DungeonManager.instance.Pink();
           // StopCoroutine(b);
        }
        else if (previousHealth > health)
        {
            
            knockback = rb.transform.position - PlayerController.instance.rb.transform.position;
            
            anim.SetTrigger("ow");
            PlayerController.instance.rb.AddForce(-knockback*250, ForceMode.Force);
            PlayerController.instance.rb.AddForce(transform.up * 5);
            AudioManager.instance.PlaySFX("knockback");
            resting = false;
            fired = 0;
            Debug.Log("Heyyo");

            timer = timeActual;
            state = State.Recovering;
        }
    }


    IEnumerator LightChange()
    {

        for (float t = 0; t < 4f; t += Time.deltaTime)
        {
            float frac = t / 4f;

            DungeonManager.instance.wizLight.color = Color.Lerp(blue, white, frac);
            yield return new WaitForEndOfFrame();
        }
           
        yield break;
    }

    IEnumerator RedLightChange()
    {

        for (float t = 0; t < 2f; t += Time.deltaTime)
        {
            float frac = t / 2f;

            DungeonManager.instance.wizLight.color = Color.Lerp(red, blue, frac);
            yield return new WaitForEndOfFrame();
        }

        yield break;
    }

   // IEnumerator MissileShoot()
   // {
   //     while (enabled)
   //     {
   //         yield return new WaitForSeconds(1);
   //        
   //     }
   //     resting = true;
   //     yield return new WaitForSeconds(8);
   //     resting = false;
   // }

   // IEnumerator Recharged()
   // {
   //    // while (enabled)
   //    //{ if (yowch) {yowch = true; yield break;} }
   //
   //   
   //     anim.SetTrigger("unReady");
   //     yield return new WaitForSeconds(8);
   //     anim.SetTrigger("ready");
   //     resting = false;
   //     state = State.Idle;
   //     fired = 0;
   //     Debug.Log("Heyyo");
   //     yield break;
   // }

    

    
}
