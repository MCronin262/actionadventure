﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobController : MonoBehaviour {


    Rigidbody rb;

    Transform player;
    Animator anim;

    public float chaseDist = 5;
    public float attackDist = 1;
    public float speed = 2;
    public float attackStart;
    public float turnSpeed = 2;
    public HealthController health;

    public enum State
    {
        Idle,
        Chase,
        Attack,
        Recovering,
        Dead
    }
    public State state = State.Idle;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = PlayerController.instance.transform;
        anim = GetComponent<Animator>();
       // health = GetComponent<HealthController>();
    }

    void OnEnable()
    {
        health.onHealthChanged += HealthChanged;
    }

    void OnDisable()
    {
        health.onHealthChanged -= HealthChanged;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Chase:
                ChaseUpdate();
                break;
            case State.Attack:
          //      AttackUpdate();
                break;
            case State.Recovering:
                RecoveringUpdate();
                break;
            case State.Dead:
                DeadUpdate();
                break;
            default:
                Debug.LogWarning("Undefined enemy state");
                break;

        }
    }

    void IdleUpdate()
    {
        rb.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist < chaseDist)
        {
            state = State.Chase;
        }
    }
    void ChaseUpdate()
    {
        float dist = Vector3.Distance(transform.position, player.position);
        Vector3 dir = (player.position - transform.position).normalized;
        Vector3 cross = Vector3.Cross(-transform.forward, dir);
        transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);

        if (dist > chaseDist)
        {
            state = State.Idle;
        }
        else if (dist < attackDist)
        {
            anim.SetTrigger("attack");
            
            rb.velocity = Vector3.zero;
            state = State.Attack;
            attackStart = Time.time;
        }
        else
        {
            
            rb.velocity = dir * speed;
        }
    }
    //  void AttackUpdate()
    //  {
    //
    //      Vector3 dir = (player.position - transform.position).normalized;
    //      rb.velocity = -dir * speed * 0.5f;
    //      rb.velocity = Vector3.zero;
    //
    //      if (Time.time - attackStart > 2)
    //      {
    //          state = State.Idle;
    //      }

    
    void RecoveringUpdate()
    {

    }
    void DeadUpdate()
    {
        gameObject.layer = 12;
    }

    public void BlobAttack()
    {
        AudioManager.instance.PlaySFX("BlobAttack");
    }

    public void AttackFinished()
    {
        state = State.Idle;
    }

    void HealthChanged(float previousHealth, float health)
    {
        if (previousHealth > 0 && health == 0)
        {
            anim.SetTrigger("death");
            state = State.Dead;
            AudioManager.instance.PlaySFX("BlobDeath");
        }
        else if (previousHealth > health)
        {
            anim.SetTrigger("hurt");
            AudioManager.instance.PlaySFX("BlobDeath");
        }
    }
}
