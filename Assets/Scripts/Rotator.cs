﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.tag == "trap") { transform.Rotate( 0, 0, -50 * Time.deltaTime); }
       
        else
        {
            transform.Rotate(0, -50 * Time.deltaTime, 0);
        }
    }
}
