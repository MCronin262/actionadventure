﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour {

   // public static MissileController instance;

    public Rigidbody body;
    public float speed = 3;
   // public Transform target;
    public float deeps = 0.5f;
    float playerHeight;
    Transform player;



    private void Start()
    {
        body = GetComponent<Rigidbody>();
        player = PlayerController.instance.transform;
        playerHeight = PlayerController.instance.GetComponent<CapsuleCollider>().bounds.extents.y;
        //  Sombrero();
    }
    public void Sombrero(Vector3 forward)
    { 
       
      //  Vector3 dir = (player.transform.position + Vector3.up * playerHeight - transform.position).normalized;
       // body.velocity = dir.normalized * speed;
        StartCoroutine(Fwoosh());

        transform.forward = forward;
        body.velocity = forward * 7;

        // transform.LookAt(PlayerController.instance.transform.position);

    }

    //  private void OnDisable()
    //  {
    //      body.velocity = Vector3.zero;
    //  }
    //
    //  private void OnEnable()
    //  {
        
        //Sombrero();
     // }

    void Update()
    {
         player.transform.position= PlayerController.instance.transform.position ;
       transform.Rotate(0, -280 * Time.deltaTime, 0);
    }

    private void OnCollisionEnter(Collision c)
    {
        StartCoroutine(Vanish());
    }


    IEnumerator Fwoosh()
    {

        while (enabled)
        {
            yield return new WaitForSeconds(3f);
            StartCoroutine(Vanish());
        }
    }

    IEnumerator Vanish()
    {
        GameObject plasma = Spawner.instance.Spawn("Plasma");
        plasma.transform.position = gameObject.transform.position;
        
        yield return new WaitForEndOfFrame();
       // body.velocity = Vector3.zero;
        gameObject.SetActive(false);
        yield break;
    }
}
