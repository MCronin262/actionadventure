﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour {

    public float damage = 1;

    void HitObject (GameObject g)
    {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null)
        {
            health.TakeDamage(damage);

        }
    }

    private void OnCollisionEnter(Collision c)
    {
        HitObject(c.gameObject);
        
    }
    private void OnTriggerEnter(Collider c)
    {
        HitObject(c.gameObject);
        
    }
}
