﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vanish : MonoBehaviour {

    private void OnEnable()
    {
        StartCoroutine(Hide());

    }

    IEnumerator Hide()
    {
        yield return new WaitForSeconds(0.5f);
        //transform.localScale = new Vector3(1, 1, 1);
        gameObject.SetActive(false);
    }
}
