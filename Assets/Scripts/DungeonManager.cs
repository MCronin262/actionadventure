﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DungeonManager : MonoBehaviour {

    public static DungeonManager instance;
    public GameObject door1;
    public GameObject door2;
    public int keyCount = 0;
    public Light wizLight;
    public Image whiteOut;
    public Color campt;

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            
        }
       // campt = Color.clear;
        
    }


    void Update ()
    {
		if (keyCount == 1)
        {
            door1.GetComponent<Animator>().SetBool("Trigger", true);
        }
        if (keyCount == 2)
        {
            door2.GetComponent<Animator>().SetBool("Trigger", true);
        }
        whiteOut.color = campt;
    }

    public void Pink() { StartCoroutine(Winner()); }

    IEnumerator Winner()
    {
        yield return new WaitForSeconds(3);
        for (float t = 0; t < 5f; t += Time.deltaTime)
        {
            float frac = t / 5f;

            campt = Color.Lerp( Color.clear, Color.white,frac);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Main Menu");
    }

}
