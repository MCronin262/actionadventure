﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatController : MonoBehaviour {

    Rigidbody rb;
    Transform player;
    Animator anim;

    public float chaseDist = 5;
    public float attackDist = 1;
    public float speed = 2;
    public float attackStart;
    public float turnSpeed = 2;    
    Vector3 homePosition;
    float playerHeight;
   public HealthController health;
    int pow = 0;

    public enum State
    {
        Idle,
        Chase,
        Attack,
        Return,
        Dead
    }
    public State state = State.Idle;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = PlayerController.instance.transform;
        anim = GetComponent<Animator>();
        homePosition = transform.position;
        playerHeight = player.GetComponent<CapsuleCollider>().bounds.extents.y;
       // health = GetComponent<HealthController>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Chase:
                ChaseUpdate();
                break;
            case State.Attack:
                AttackUpdate();
                break;
            case State.Return:
                ReturnUpdate();
                break;
            case State.Dead:
                DeadUpdate();
                break;
            default:
                Debug.LogWarning("Undefined enemy state");
                break;
        }
        anim.SetFloat("speed", rb.velocity.magnitude);
    }

    void OnEnable()
    {
        if (health == null) Debug.Log("Health is null");
        health.onHealthChanged += HealthChanged;
    }

    void OnDisable()
    {
        health.onHealthChanged -= HealthChanged;
    }


    void IdleUpdate()
    {
        rb.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist < chaseDist)
        {
            state = State.Chase;
            anim.SetTrigger("charge");
        }
    }
    void ChaseUpdate()
    {
        
        Vector3 dir = (player.position +Vector3.up*playerHeight- transform.position).normalized;
        Vector3 cross = Vector3.Cross(-transform.forward, dir);
        transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);        
        rb.velocity = dir * speed;
        //transform.LookAt(player);
    }
    void AttackUpdate()
    {
        rb.velocity = Vector3.zero;
        //  rb.velocity = (transform.forward+Vector3.up).normalized * speed;
        // float dist = Vector3.Distance(player.position, transform.position);
        // if (dist >attackDist)
        // {
        //     state = State.Return;
        //     anim.SetTrigger("retreat");
        // }
    }

    void ReturnUpdate()
    {
        Vector3 dir = (homePosition - transform.position).normalized;
        Vector3 cross = Vector3.Cross(-transform.forward, dir);
        transform.Rotate(Vector3.up * cross.y * turnSpeed * Time.deltaTime);
        rb.velocity = dir * speed;
        //transform.LookAt(player);


        float dist = Vector3.Distance(homePosition, transform.position);
        if (dist < .2f)
        {
            state = State.Idle;
            anim.SetTrigger("retreat");
            pow = 0;
        }
    }

    public void AttackFinished()
    {
        state = State.Return;
        anim.SetTrigger("retreat");
    }


    void DeadUpdate()
    {
        gameObject.layer = 12;
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player" && pow==0)
        {
            state = State.Attack;
            anim.SetTrigger("attack");
            AudioManager.instance.PlaySFX("BatAttack");
            pow = 1;
        }
    }


    void HealthChanged(float previousHealth, float health)
    {
        if (previousHealth > 0 && health == 0)
        {
            anim.SetTrigger("Death");
            state = State.Dead;
            AudioManager.instance.PlaySFX("BatDeath");
            rb.velocity = Vector3.zero;
        }
        else if (previousHealth > health)
        {
            anim.SetTrigger("hurt");
            AudioManager.instance.PlaySFX("BatPain");
            state = State.Return;
            
        }
    }

}
