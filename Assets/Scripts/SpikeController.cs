﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeController : MonoBehaviour {

    //Rigidbody rb;    
    Animator anim;
     void Start()
    {
       // rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            AudioManager.instance.PlaySFX("Spike");
            anim.SetTrigger("up");
        }
    }
}
