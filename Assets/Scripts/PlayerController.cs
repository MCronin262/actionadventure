﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public static PlayerController instance;
    Animator anim;
   public Rigidbody rb;
    public float fowardSpeed = 10;
    public float turnSpeed = 5;
    public float maxSpeedChange = 1;
    bool dead=false;
    public bool paused = false;
    public bool weakspot = false;

    public bool crouched = false;
    public bool jumping = false;
    public HealthController health;
    public bool wizDead = false;

    

    private void Awake()
    {
        if (instance==null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        health = GetComponent<HealthController>();
        

        

        if (GameManager.instance.exit)
        {
            transform.position = new Vector3(-238, 38, 443);
            transform.eulerAngles = new Vector3(0,90,0);
        }
        else
        {
          //  transform.position = new Vector3(1, 0, -9);
           // transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    
    private void OnEnable()
    {
        health.onHealthChanged += HealthChanged;
    }

    private void OnDisable()
    {
        health.onHealthChanged += HealthChanged;
    }

    private void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");


        // anim.SetFloat("s", x);
        // rb.velocity = transform.forward * y * fowardSpeed;
        // rb.angularVelocity = Vector3.up * x * turnSpeed;

        Vector3 v = transform.forward * y * fowardSpeed;
        v.y = rb.velocity.y;
        Vector3 change = v - rb.velocity;
        if (change.magnitude > maxSpeedChange) change = change.normalized * maxSpeedChange;
        rb.AddForce(change, ForceMode.VelocityChange);
        //rb.velocity = v;
        rb.angularVelocity = Vector3.up * x * turnSpeed;

       
        anim.SetFloat("x", rb.angularVelocity.y);
        anim.SetFloat("y", Vector3.Dot(rb.velocity, transform.forward));

        if (!crouched && !dead&&!paused)
        {
            if (Input.GetButtonDown("Jump")&&!paused)
            {
               
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
                { }
                else
                {
                   // anim.Play("Jump");
                   // rb.AddForce(transform.up * 210);                    
                    // anim.applyRootMotion = false;
                }                
            }

            if (Input.GetButtonDown("Fire1"))
            {
               // rb.velocity = Vector3.zero;
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Kick"))
                { }
                else { anim.SetTrigger("attack"); }
                    
            }
            if (Input.GetButtonDown("Fire2"))
            {
                anim.Play("Grab Tree");
            }

            if (Input.GetButtonDown("OneButton"))
            { anim.Play("Golf");}
            if (Input.GetButtonDown("TwoButton"))
            { anim.Play("Look");}
            if (Input.GetButtonDown("ThreeButton"))
            {
                if (Random.Range(0, 2) == 1)
                { anim.Play("Point"); }
                else
                { anim.Play("Pockets"); }
            }
        }

        if (Input.GetButtonDown("Fire3"))
        {
            StartCoroutine(CControl());
        }

        
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Kick"))
        {
           // rb.velocity = v/2;
        }
        

    }

    


    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "BallReturn")
        {
            gameObject.transform.position = new Vector3(1, .2f, -9);
        }
        if (c.gameObject.tag == "ExitDoor")
        {
            SceneManager.LoadScene("Prime");
            GameManager.instance.exit = true;

            //transform.position = new Vector3(-238, 38, 443);
            //transform.eulerAngles = new Vector3(0, 90, 0);
                       
        }
        if (c.gameObject.tag == "Entrydoor")
        {
            GameManager.instance.carryOver = true;
            SceneManager.LoadScene("Dungeon");
            GameManager.instance.exit = false;

           
            // transform.position = new Vector3(1, 0, -9);
            //transform.eulerAngles = new Vector3(0, 0, 0);

        }
        if (c.gameObject.tag == "Button")
        {
            c.gameObject.SetActive(false);
            DungeonManager.instance.keyCount++;
            AudioManager.instance.PlaySFX("button");
        }

        if (c.gameObject.tag == "weakspot")
        {
            weakspot = true;
        }

        if (c.gameObject.tag == "trap")
        {
            c.gameObject.SetActive(false);
            AudioManager.instance.PlaySFX("Death");
        }
    }

    private void OnTriggerExit(Collider c)
    {
        if (c.gameObject.tag == "weakspot")
        {
            weakspot = false;
        }

    }

    IEnumerator CControl()
    {
        if (!crouched)
        {
            fowardSpeed = fowardSpeed / 2;
            crouched = true;
            anim.Play("Crouching");
            yield return new WaitForSeconds(.1f);
            yield break;
        }

        if (crouched)
        {
            fowardSpeed = fowardSpeed * 2;
            crouched = false;
            anim.Play("Uncrouch");
            yield return new WaitForSeconds(.1f);
            yield break;
        }
    }


    void HealthChanged(float previousHealth, float health)
    {
        if (previousHealth > 0 && health==0)
        {
            anim.SetTrigger("death");
            dead = true;
            AudioManager.instance.PlaySFX("Heartbeat");
            AudioManager.instance.PlaySFX("Death");
            StartCoroutine (Deathed());
        }
       else if (previousHealth > health)
        {
            anim.SetTrigger("ow");
            AudioManager.instance.PlaySFX("NinjaDeath");
        }
    }


    IEnumerator Deathed()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Main Menu");
    }

   


}



