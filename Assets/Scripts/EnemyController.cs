﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {


    Rigidbody rb;

    Transform player;

    public float chaseDist=5;
    public float attackDist = 1;
    public float speed = 2;
    public float attackStart;

    public enum State
    {
        Idle,
        Chase,
        Attack,
        Recovering,
        Dead
    }
    public State state = State.Idle;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        player = PlayerController.instance.transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
		switch (state)
        {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Chase:
                ChaseUpdate();
                break;
            case State.Attack:
                AttackUpdate();
                break;
            case State.Recovering:
                RecoveringUpdate();
                break;
            case State.Dead:
                DeadUpdate();
                break;
            default:
                Debug.LogWarning("Undefined enemy state");
                break;
                
        }
	}

   void IdleUpdate()
    {
        rb.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist < chaseDist)
        {
            state = State.Chase;
        }
    }
    void ChaseUpdate()
    {
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist > chaseDist)
        {
            state = State.Idle;
        }
        else if (dist < attackDist)
        {
            state = State.Attack;
            attackStart = Time.time;
        }
        else
        {
            Vector3 dir = (player.position - transform.position).normalized;
            rb.velocity = dir * speed;
        }
    }
    void AttackUpdate()
    {
        
        Vector3 dir = (player.position - transform.position).normalized;
        rb.velocity = -dir * speed*0.5f;
        
        if (Time.time - attackStart > 2)
        {
            state = State.Idle;
        }
    }
    void RecoveringUpdate()
    {

    }
    void DeadUpdate()
    {

    }
}
