﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextController : MonoBehaviour {


    public static UITextController instance;

    public GameObject textBox;
    public GameObject promptBox;
    public Text signText;
    public Text promptText;

   
    public State state = State.Hidden;

    private string[] aText;
    private int currentMessage;

    public enum State
    {
        Hidden,
        Prompting,
        Visible,
        Done
    }
    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            
        }
    }


    private void Update()
    {
        switch (state) {
            case State.Hidden:
                promptBox.SetActive(false);
                textBox.SetActive(false);
                break;
            case State.Prompting:
                PromptUpdate();
                break;
            case State.Visible:
                VisibleUpdate();
                break;
            case State.Done:
                DoneUpdate();
                break;
            default:
                break;
                
        }
    }


    public static void Show(string[] messages)
    {
        
        instance.aText = messages;
        
        instance.ShowPrompt();

        
    }

    public static void Hide()
    {
        instance.state = State.Hidden;
    }
    
    void ShowPrompt()
    {
        state = State.Prompting;
        currentMessage = 0;
        textBox.SetActive(false);
        promptBox.SetActive(true);
        promptText.text = "Read";
    }

    void PromptUpdate()
    {
        if (Input.GetButtonDown("Jump"))
        {
            state = State.Visible;
            textBox.SetActive(true);
            promptText.text = "Next";
        }

    }

    void VisibleUpdate()
    {
        signText.text=aText[currentMessage];

             

        if (Input.GetButtonDown("Jump"))
        {
            currentMessage++;
            //currentMessage = Mathf.Clamp(currentMessage, 0, aText.Length - 1);
            if (currentMessage >= aText.Length -1)
            {
                state = State.Done;
                promptText.text = "Done";
                signText.text = aText[aText.Length-1];
            }
        }

    }

    void DoneUpdate()
    {
        if (Input.GetButtonDown("Jump"))
        {
            ShowPrompt();
        }
    }

    IEnumerator DisplayMessages()
    {
        textBox.SetActive(true);
        for (int i = 0; i < aText.Length; i++)
        {
            promptBox.SetActive(false);
            bool isLast = (i == aText.Length - 1);
            yield return StartCoroutine(DisplayMessage(aText[i], isLast));   
        }
    }

    IEnumerator DisplayMessage(string message, bool isLast)
    {
        string s = "";
        for (int i = 0; i < aText.Length; i++)
        {
            s += message[i];
            signText.text = s;
            if (Input.GetButton("Fire2")) yield return new WaitForEndOfFrame();
            else yield return new WaitForSeconds(.3f);
        }
        yield return new WaitUntil(() => !Input.GetButton("Jump"));
        promptBox.SetActive(true);
        if (isLast) promptText.text = "Done";
        else promptText.text = "Next";
        yield return new WaitUntil(() => Input.GetButton("Jump"));
    }
}
